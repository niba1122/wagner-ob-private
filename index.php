<?php
require_once 'google-api-php-client-2.1.3/vendor/autoload.php';

date_default_timezone_set('Asia/Tokyo');

session_start();

function oauth2_callback_url() {
  return 'http://' . $_SERVER['HTTP_HOST'] . '/oauth2callback.php';
}

function content_url($id) {
  return 'http://' . $_SERVER['HTTP_HOST'] . '/?folder_id=' . $id;
}

function logout_url() {
  return 'http://' . $_SERVER['HTTP_HOST'] . '/?mode=logout';
}

function redirect_to_login() {
  $redirect_uri = oauth2_callback_url();
  header('Location: ' . filter_var($redirect_uri, FILTER_SANITIZE_URL));
  exit(0);
}

function get_files($drive, $folder_id) {
  $optParams = array(
    'pageSize' => 100,
    'orderBy' => 'createdTime desc',
    'fields' => "nextPageToken, files(id, name, webContentLink)",
    'q' => "mimeType != 'application/vnd.google-apps.folder' and '" . $folder_id . "' in parents"
  );
  return $drive->files->listFiles($optParams)->getFiles();
}

function get_folder_tree($drive) {
  $optParams = array(
    'pageSize' => 50,
    'fields' => "nextPageToken, files(id, name, owners, parents, webContentLink)",
    'q' => "mimeType = 'application/vnd.google-apps.folder' and 'wagner.ob.schedule@gmail.com' in owners"
  );

  $response = $drive->files->listFiles($optParams)->getFiles();

  $folders = array();
  $id2folder = array();
  foreach ($response as $response_folder) {
    $folder = (object) array(
      'id' => $response_folder->id,
      'name' => $response_folder->name,
      'children' => array()
    );
    $folders[] = $folder;
    $id2folder[$folder->id] = $folder;
  }

  foreach ($response as $response_folder) {
    $parents = $response_folder->parents;
    if ($parents) {
      foreach ($parents as $parent_id) {
        $parent = $id2folder[$parent_id];
        if ($parent) {
          $folder = $id2folder[$response_folder->id];
          $parent->children[] = $folder;
        }
      }
    }
  }

  $root_folders = array();
  foreach ($response as $response_folder) {
    if (!$response_folder->parents) {
      $folder = $id2folder[$response_folder->id];
      $root_folders[] = $folder;
    }
  }

  return $root_folders;
}

function get_folder($drive, $id) {
  return $drive->files->get($id);
}

if ($_GET['mode'] == 'logout') {
  $_SESSION['access_token'] = null;
}

if (!isset($_SESSION['access_token']) || !$_SESSION['access_token']) {
  redirect_to_login();
}

$client = new Google_Client();
$client->setAuthConfig('client_secrets.json');
$client->addScope(Google_Service_Drive::DRIVE_METADATA_READONLY);
$client->setAccessToken($_SESSION['access_token']);
if ($client->isAccessTokenExpired()) {
  $_SESSION['access_token'] = null;
  redirect_to_login();
}

$drive = new Google_Service_Drive($client);


?>
<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
<link rel="stylesheet" href="https://code.getmdl.io/1.3.0/material.indigo-pink.min.css">
<script defer src="https://code.getmdl.io/1.3.0/material.min.js"></script>
<style>
.wso-drop-down-container {
  display: none;
}
.wso-drop-down-container.open {
  display: block;
}

.mdl-layout__content {
  padding: 16px;
}

.mdl-navigation__link {
}

</style>
</head>
<body>
<div class="mdl-layout mdl-js-layout mdl-layout--fixed-drawer mdl-layout--fixed-header">
  <header class="mdl-layout__header">
    <div class="mdl-layout__header-row">
      <span class="mdl-layout-title">WSO団員用ページ</span>
      <div class="mdl-layout-spacer"></div>
      <button class="mdl-button mdl-js-button mdl-button--icon wso-sign-out-button">
        <i class="material-icons">exit_to_app</i>
      </button>
    </div>
  </header>
  <div class="mdl-layout__drawer">
    <span class="mdl-layout-title">Contents</span>
    <nav class="mdl-navigation">
<?php

$folder_tree = get_folder_tree($drive);

function render_folder_tree($folder_tree, $nest_count = 0) {
  $material_icon_width = 24;
  foreach ($folder_tree as $folder) {
    if (count($folder->children) > 0) {
?>
      <a href="javascript:void(0)" class="mdl-navigation__link wso-drop-down-toggle-button">
        <span style="display: block; padding-left: <?php echo $material_icon_width * $nest_count; ?>px;">
          <i class="material-icons wso-drop-down-material-icon">arrow_drop_down</i><?php echo $folder->name; ?>
        </span>
      </a>
      <div class="wso-drop-down-container">
<?php
      render_folder_tree($folder->children, $nest_count + 1);
?>
      </div>
<?php
    } else {
?>
      <a href="<?php echo content_url($folder->id); ?>" class="mdl-navigation__link">
        <span style="display: block; padding-left: <?php echo $material_icon_width * ($nest_count + 1); ?>px;">
          <?php echo $folder->name; ?>
        </span>
      </a>
<?php
    }
  }
}

render_folder_tree($folder_tree);

?>
    </nav>
  </div>
  <main class="mdl-layout__content">
<div class="mdl-card mdl-shadow--2dp" style="width: 100%;">
<?php
if ($_GET['folder_id']) {
  $folder = get_folder($drive, $_GET['folder_id']);
  $files = get_files($drive, $_GET['folder_id']);
?>
    <div class="mdl-card__title">
      <h2 class="mdl-card__title-text"><?php echo $folder->name; ?></h2>
    </div>
    <ul class="mdl-list">
<?php

  foreach ($files as $file) {
    $file_url = $file->webContentLink ? $file->webContentLink : $file->webViewLink;
?>
      <li class="mdl-list__item">
        <a class="mdl-list__item-primary-content" href="<?php echo $file_url;?>" target="_blank"><?php echo $file->name; ?></a>
      </li>
<?php
  }
?>
    </ul>
<?php
}
?>
</div>
  </main>
</div>
<script>
Array.prototype.forEach.call(document.querySelectorAll('.wso-drop-down-toggle-button'), function (buttonDOM) {
  buttonDOM.addEventListener('click', function() {
    if (!this.nextElementSibling.classList) return;
    this.nextElementSibling.classList.toggle('open');
    if (!this.querySelector('.wso-drop-down-material-icon')) return;
    if (this.nextElementSibling.classList.contains('open')) {
      this.querySelector('.wso-drop-down-material-icon').innerHTML = 'arrow_drop_up';
    } else {
      this.querySelector('.wso-drop-down-material-icon').innerHTML = 'arrow_drop_down';
    }
  });
});
Array.prototype.forEach.call(document.querySelectorAll('.wso-sign-out-button'), function (buttonDOM) {
  buttonDOM.addEventListener('click', function() {
    location.href = '<?php echo logout_url(); ?>';
  });
});
</script>
</body>
</html>
